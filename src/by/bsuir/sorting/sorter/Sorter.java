package by.bsuir.sorting.sorter;

public interface Sorter {

    void sort(int[] input);
    
    String getName();
}