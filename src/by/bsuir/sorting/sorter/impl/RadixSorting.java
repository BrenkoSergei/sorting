package by.bsuir.sorting.sorter.impl;

import java.util.Arrays;

import by.bsuir.sorting.sorter.Sorter;

public class RadixSorting implements Sorter {

    @Override
    public void sort(int[] input) {
        int n = input.length;
        int m = getMax(input, n);
        for (int exp = 1; m / exp > 0; exp *= 10) {
            countSort(input, n, exp);
        }
    }

    @Override
    public String getName() {
        return "Корневая Сортировка";
    }

    private int getMax(int[] arr, int n) {
        int max = arr[0];
        for (int i = 1; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    private static void countSort(int arr[], int n, int exp) {
        int output[] = new int[n]; // output array
        int i;

        int count[] = new int[19];
        Arrays.fill(count, 0);
        for (i = 0; i < n; ++i) {
            count[(arr[i] / exp) % 10 + 9]++;
        }
        for (i = 1; i < count.length; ++i) {
            count[i] += count[i - 1];
        }

        for (i = n - 1; i >= 0; --i) {
            output[count[(arr[i] / exp) % 10 + 9] - 1] = arr[i];
            count[(arr[i] / exp) % 10 + 9]--;
        }
        for (i = 0; i < n; ++i) {
            arr[i] = output[i];
        }
    }
}
