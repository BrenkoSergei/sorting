package by.bsuir.sorting.sorter.impl;

import by.bsuir.sorting.sorter.Sorter;

public class BinarySorting implements Sorter {

    @Override
    public void sort(int[] a) {
        int n = a.length;
        for (int k = n / 2; k > 0; k--) {
            downheap(a, k, n);
        }
        do {
            int temp = a[0];
            a[0] = a[n - 1];
            a[n - 1] = temp;
            n = n - 1;
            downheap(a, 1, n);
        } while (n > 1);
    }

    @Override
    public String getName() {
        return "Пирамидальная Сортировка";
    }

    private void downheap(int a[], int k, int N) {
        int temp = a[k - 1];
        while (k <= N / 2) {
            int j = k + k;
            if ((j < N) && (a[j - 1] < a[j])) j++;
            if (temp >= a[j - 1]) {
                break;
            } else {
                a[k - 1] = a[j - 1];
                k = j;
            }
        }
        a[k - 1] = temp;
    }
}
