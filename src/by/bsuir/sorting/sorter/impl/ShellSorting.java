package by.bsuir.sorting.sorter.impl;

import by.bsuir.sorting.sorter.Sorter;

public class ShellSorting implements Sorter {

    @Override
    public void sort(int[] input) {
        int step = input.length / 2;
        while (step > 0) {
            for (int i = 0; i < (input.length - step); i++) {
                for (int j = i; j >= 0 && input[j] > input[j + step]; j--) {
                    int t = input[j + step];
                    input[j + step] = input[j];
                    input[j] = t;
                }
            }
            step = step / 2;
        }
    }

    @Override
    public String getName() {
        return "Метод Шелла";
    }
}
