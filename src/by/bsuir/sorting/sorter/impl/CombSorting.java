package by.bsuir.sorting.sorter.impl;

import by.bsuir.sorting.sorter.Sorter;

public class CombSorting implements Sorter {

    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        int gap = n;
        boolean swapped = true;
        while (gap != 1 || swapped) {
            gap = getNextGap(gap);
            swapped = false;
            for (int i = 0; i < n - gap; i++) {
                if (arr[i] > arr[i + gap]) {
                    int temp = arr[i];
                    arr[i] = arr[i + gap];
                    arr[i + gap] = temp;
                    swapped = true;
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Сортировка Расческой";
    }

    private static int getNextGap(int gap) {
        gap = (gap * 10) / 13;
        return gap < 1 ? 1 : gap;
    }
}
