package by.bsuir.sorting.sorter.impl;

import by.bsuir.sorting.sorter.Sorter;

public class MinElementSorting implements Sorter {

    @Override
    public void sort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int least = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[least]) {
                    least = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[least];
            arr[least] = temp;
        }
    }

    @Override
    public String getName() {
        return "Сортировка Выбором Наименьшего Элемента";
    }
}
