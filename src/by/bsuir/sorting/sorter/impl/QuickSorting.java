package by.bsuir.sorting.sorter.impl;

import by.bsuir.sorting.sorter.Sorter;

public class QuickSorting implements Sorter {

    @Override
    public void sort(int[] input) {
        qSort(input, 0, input.length - 1);
    }

    @Override
    public String getName() {
        return "Метод Хоара";
    }

    private static void qSort(int[] A, int low, int high) {
        int i = low;
        int j = high;
        double x = A[low + (high - low) / 2];
        do {
            while (A[i] < x) ++i;
            while (A[j] > x) --j;
            if (i <= j) {
                swap(A, i, j);
                i++;
                j--;
            }
        } while (i <= j);
        if (low < j) qSort(A, low, j);
        if (i < high) qSort(A, i, high);
    }

    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
