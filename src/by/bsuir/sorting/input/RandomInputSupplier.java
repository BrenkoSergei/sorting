package by.bsuir.sorting.input;

import java.util.Random;

public class RandomInputSupplier implements InputSupplier {

    private static final int DEFAULT_RANDOM_ORIGIN = -2_000_000;
    private static final int DEFAULT_RANDOM_BOUND = 2_000_000;

    private final int count;
    private final int randomOrigin;
    private final int randomBound;

    public RandomInputSupplier(int count) {
        this(count, DEFAULT_RANDOM_ORIGIN, DEFAULT_RANDOM_BOUND);
    }

    public RandomInputSupplier(int count, int randomOrigin, int randomBound) {
        this.count = count;
        this.randomOrigin = randomOrigin;
        this.randomBound = randomBound;
    }

    @Override
    public int[] getInputData() {
        return new Random(System.currentTimeMillis())
                .ints(count, randomOrigin, randomBound)
                .toArray();
    }
}
