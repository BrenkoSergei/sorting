package by.bsuir.sorting.input;

public interface InputSupplier {

    int[] getInputData();
}
