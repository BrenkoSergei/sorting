package by.bsuir.sorting;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.stream.Stream;

import by.bsuir.sorting.input.InputSupplier;
import by.bsuir.sorting.input.RandomInputSupplier;
import by.bsuir.sorting.sorter.impl.BinarySorting;
import by.bsuir.sorting.sorter.impl.CombSorting;
import by.bsuir.sorting.sorter.impl.MinElementSorting;
import by.bsuir.sorting.sorter.impl.QuickSorting;
import by.bsuir.sorting.sorter.impl.RadixSorting;
import by.bsuir.sorting.sorter.impl.ShellSorting;

public class Runner {

    private static final int ROW_LENGTH = 20;

    public static void main(String[] args) {
        InputSupplier inputSupplier = new RandomInputSupplier(1_000_000);
        int[] inputData = inputSupplier.getInputData();
//        printTitle("Input Data");
//        printArrayAsTable(inputData, ROW_LENGTH);

        Stream.of(
                new MinElementSorting(),
                new BinarySorting(),
                new QuickSorting(),
                new ShellSorting(),
                new CombSorting(),
                new RadixSorting()
        ).forEach(sorter -> {
            int[] dataCopy = Arrays.copyOf(inputData, inputData.length);

            long start = System.currentTimeMillis();
            sorter.sort(dataCopy);
            long finish = System.currentTimeMillis();

//            System.out.println();
            printTitle(String.format("%,d мс (%s)", finish - start, sorter.getName()));
//            printArrayAsTable(dataCopy, ROW_LENGTH);
        });
    }

    private static void printTitle(String title) {
        System.out.printf("============ %s ============\n", title);
    }

    private static void printArrayAsTable(int[] array, int rowLength) {
        for (int i = 0; i < array.length; ) {
            for (int j = 0; j < rowLength && i < array.length; j++) {
                System.out.printf("%4d ", array[i++]);
            }
            System.out.println();
        }
    }
}
